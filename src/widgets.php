<?php

/**
 * Register events widget. Generate random id to widget block.
 */
Widget::register('events_chart', function(){

	$widget_id = 'events-chart-'.md5(microtime(true));

	return View::make('charts::widgets.events-chart', compact('widget_id'));

});
