
<!-- Portlet: Browser Usage Graph-->
<div class="box events-chart-box" id="box-{{ $widget_id }}">
	<h4 class="box-header round-top">Events Graph
		<a class="box-btn" title="close"><i class="icon-remove"></i></a>
		<a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
		<a class="box-btn" title="config" data-toggle="modal" href="#"><i class="icon-cog"></i></a>
	</h4>
	<div class="box-container-toggle">
		<div class="graph-menu">
			<div class="graphs-calendar">
				<div class="graphs-calendar-period">
					<input class="date-picker-inputs" id="date-picker-input-start" type="hidden">
					<input class="date-picker-inputs" id="date-picker-input-end" type="hidden">
					<div class="period-btn">Period</div>
				</div>
				<div class="date-picker-block">
					<span class="date-picker-close">apply</span>
					<div class="datepicker"></div>
				</div>
			</div>
			<div class="graph-menu-filters">
				<div class="controls">
					<select class="filter-unit">
						<option data-filter="hour">Hour</option>
						<option data-filter="day" selected="selected">Day</option>
						<option data-filter="week">Week</option>
						<option data-filter="month">Month</option>
					</select>
					<select class="filter-type">
						<option data-filter="unique">Unique</option>
						<option data-filter="general" selected="selected">Total</option>
						<option data-filter="average">Average</option>
					</select>
				</div>
			</div>
		</div>
		<div class="box-content chart-block">
			<div class="nodata"></div>
			<div class="chart-container"></div>
		</div>
	</div>
</div>
<!--/span-->
