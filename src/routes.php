<?php

Route::get('test',  'In\Charts\ChartsController@showChart');

Route::get('chart',  'In\Charts\ChartsController@showChart');
Route::get('chart2',  'In\Charts\ChartsController@showChart');
Route::post('charts/getEventsChartData',  'In\Charts\ChartsController@getEventsChartData');