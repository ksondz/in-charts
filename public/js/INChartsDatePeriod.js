var INChartsDatePeriod = (function() {

    function getDateRangeFromDaysNumber(daysCount) {

        var datesRange = {
            start : {
                format : {}
            },
            end : {
                format : {}
            }
        };

        var todaysDateInLocalTimezone = new Date();

        var endDateInUTC = new Date(
            todaysDateInLocalTimezone.getUTCFullYear(),
            todaysDateInLocalTimezone.getUTCMonth(),
            todaysDateInLocalTimezone.getUTCDate(),
            todaysDateInLocalTimezone.getUTCHours(),
            todaysDateInLocalTimezone.getUTCMinutes(),
            todaysDateInLocalTimezone.getUTCSeconds()
        );
        var startDateInUTC = new Date(
            todaysDateInLocalTimezone.getUTCFullYear(),
            todaysDateInLocalTimezone.getUTCMonth(),
            todaysDateInLocalTimezone.getUTCDate(),
            todaysDateInLocalTimezone.getUTCHours(),
            todaysDateInLocalTimezone.getUTCMinutes(),
            todaysDateInLocalTimezone.getUTCSeconds()
        );

        startDateInUTC.setDate(endDateInUTC.getDate() - daysCount);

        datesRange.end.year     = endDateInUTC.getFullYear();
        datesRange.end.month    = endDateInUTC.getMonth() + 1;
        datesRange.end.month    = datesRange.end.month > 9 ? datesRange.end.month : '0' + datesRange.end.month;
        datesRange.end.day      = endDateInUTC.getDate();
        datesRange.end.day      = datesRange.end.day > 9 ? datesRange.end.day : '0' + datesRange.end.day;

        datesRange.start.year   = startDateInUTC.getFullYear();
        datesRange.start.month  = startDateInUTC.getMonth() + 1;
        datesRange.start.month  = datesRange.start.month > 9 ? datesRange.start.month : '0' + datesRange.start.month;
        datesRange.start.day    = startDateInUTC.getDate();
        datesRange.start.day    = datesRange.start.day > 9 ? datesRange.start.day : '0' + datesRange.start.day;

        datesRange.start.format.calendar = datesRange.start.month + '/' + datesRange.start.day + '/' + datesRange.start.year;
        datesRange.start.format.value = datesRange.start.year + '-' + datesRange.start.month + '-' + datesRange.start.day;

        datesRange.end.format.calendar = datesRange.end.month + '/' + datesRange.end.day + '/' + datesRange.end.year;
        datesRange.end.format.value = datesRange.end.year + '-' + datesRange.end.month + '-' + datesRange.end.day;

        return datesRange;

    };

    function bindCloseDatepickerOnOutsideClick(widgetContainer) {

        $(document).click(function(event) {
            var target = $(event.target);
            var datePicker = widgetContainer.find(".datepicker");

            if( (datePicker.css("display") == 'block') && (target.parent('.date-picker-block').length == 0) && !target.hasClass('period-btn') ) {
                datePicker.hide();
                widgetContainer.find(".date-picker-close").hide();
            }

        });

    };

    function initPeriodFilterButtonWithDefaultDateRange(widgetContainer, defaultDateRange) {
        var periodBtn = widgetContainer.find('.period-btn');
        periodBtn.
            click(function(){
                widgetContainer.find( ".datepicker" ).show();
                widgetContainer.find(".date-picker-close").show();
            })
            .text(defaultDateRange.start.format.value + ' to ' + defaultDateRange.end.format.value);
    };


    /*** DatePicker Handlers **/

    function onDateSelectHandler(selectedDateVisualRepresentation, instance, widgetContainer, todaysDateinUTC) {
        var startDateVisualRepresentation  = widgetContainer.attr('data-format-calendar-start');
        var endDateVisualRepresentation    = widgetContainer.attr('data-format-calendar-end');

        var startDateVisualRepresentation = $.datepicker.parseDate($.datepicker._defaults.dateFormat, startDateVisualRepresentation);
        var endDateVisualRepresentation = $.datepicker.parseDate($.datepicker._defaults.dateFormat, endDateVisualRepresentation);

        if(selectedDateVisualRepresentation){
            var reg = /(\d+)\/(\d+)\/(\d+)/;
            var dateParts = reg.exec(selectedDateVisualRepresentation);
            var selectDateValue = dateParts[3] + '-' + dateParts[1]  + '-' + dateParts[2];
        }

        var periodBtn = widgetContainer.find('.period-btn');


        $(this).datepicker("option", "maxDate", todaysDateinUTC);

        if (!startDateVisualRepresentation || endDateVisualRepresentation) {

            widgetContainer.attr('data-format-calendar-start-value', selectDateValue);
            widgetContainer.attr('data-format-calendar-end-value', "");

            widgetContainer.attr('data-format-calendar-start', selectedDateVisualRepresentation);
            widgetContainer.attr('data-format-calendar-end', "");

            periodBtn.text('( ' + selectDateValue + ' to  )');

            $(this).datepicker("option", "minDate", selectedDateVisualRepresentation);

        } else {

            widgetContainer.attr('data-format-calendar-end-value',selectDateValue);
            widgetContainer.attr('data-format-calendar-end',selectedDateVisualRepresentation);

            periodBtn.text(widgetContainer.attr('data-format-calendar-start-value') + ' to ' + selectDateValue);

            $(this).datepicker("option", "minDate", null);

        }

        periodBtn.attr('data-state','change');
    };

    function onBeforeShowDayHandler(displayingDate, widgetContainer) {
        var inputStart  = widgetContainer.attr('data-format-calendar-start');
        var inputEnd    = widgetContainer.attr('data-format-calendar-end');

        var periodStartDate = $.datepicker.parseDate($.datepicker._defaults.dateFormat, inputStart);
        var periodEndDate   = $.datepicker.parseDate($.datepicker._defaults.dateFormat, inputEnd);

        return [true, periodStartDate && (
            (displayingDate.getTime() == periodStartDate.getTime()) ||
                (periodEndDate && displayingDate >= periodStartDate && displayingDate <= periodEndDate)) ? "dp-highlight" : ""];
    };

    return {

        initChartPeriodDatepicker : function(widgetContainer) {
            var DEFAULT_DATE_RANGE_DAYS_COUNT = 7;

            var todaysDateInLocalTimezone = new Date();
            var todaysDateinUTC = new Date(
                todaysDateInLocalTimezone.getUTCFullYear(),
                todaysDateInLocalTimezone.getUTCMonth(),
                todaysDateInLocalTimezone.getUTCDate(),
                todaysDateInLocalTimezone.getUTCHours(),
                todaysDateInLocalTimezone.getUTCMinutes(),
                todaysDateInLocalTimezone.getUTCSeconds()
            );

            var defaultDateRange = getDateRangeFromDaysNumber(DEFAULT_DATE_RANGE_DAYS_COUNT);


            widgetContainer.attr('data-format-calendar-start',defaultDateRange.start.format.calendar);
            widgetContainer.attr('data-format-calendar-end',defaultDateRange.end.format.calendar);

            widgetContainer.attr('data-format-calendar-start-value',defaultDateRange.start.format.value);
            widgetContainer.attr('data-format-calendar-end-value',defaultDateRange.end.format.value);


            widgetContainer.find(".datepicker").datepicker({
                "maxDate" : todaysDateinUTC,
                beforeShowDay:  function(displayingDate) {
                    return onBeforeShowDayHandler(displayingDate, widgetContainer);
                },
                onSelect: function(dateText, instance) {
                    return onDateSelectHandler(dateText, instance, widgetContainer, todaysDateinUTC);
                }
            });

            initPeriodFilterButtonWithDefaultDateRange(widgetContainer, defaultDateRange);
            bindCloseDatepickerOnOutsideClick(widgetContainer);

        }

    }

})();